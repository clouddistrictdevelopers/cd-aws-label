output "id" {
  value       = local.id
  description = "Disambiguated ID"
}

output "project" {
  value       = local.project
  description = "Normalized project name"
}

output "environment" {
  value       = local.environment
  description = "Normalized environment"
}

output "delimiter" {
  value       = local.delimiter
  description = "Delimiter between `namespace`, `stage`, `name` and `attributes`"
}

output "attributes" {
  value       = local.attributes
  description = "Normalized attributes"
}

output "tags" {
  value       = local.tags
  description = "Normalized Tag map"
}
