#crea los .md de los módulos terraform

TFDOCS=$(shell which terraform-docs)

.PHONY : doc
doc:
	$(TFDOCS) markdown table --output-file README.md --output-mode inject .
