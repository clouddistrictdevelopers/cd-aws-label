variable "environment" {
  type        = string
  default     = ""
  description = "Environment, e.g. 'dev', 'acc', 'prd'"
}

variable "project" {
  type        = string
  default     = ""
  description = "Project name"
}

variable "delimiter" {
  type        = string
  default     = "-"
  description = "Delimiter to be used between `project`, `environment` and `attributes`"
}

variable "attributes" {
  type        = list(string)
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags (e.g. `map('BusinessUnit','XYZ')`"
}

variable "enabled" {
  type        = bool
  default     = true
  description = "Set to false to prevent the module from creating any resources"
}

variable "convert_case" {
  type        = bool
  default     = true
  description = "Convert fields to lower case"
}
